package application;
	
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
/*Layout - Contentores de Objetos : import javafx.scene.layout;
 * BorderPane - Tem 5 regi�es para dispor os objetos gr�ficos
 * VBox e HBox - organizam os objetos , vertical e horizontalmente
 * StackPane - Permite sobrepor varios objetos gr�ficos
 * GridPane - Tabela ajust�vel(grelha)
 * FlowPane - Alinha os objetos vert/horiz. e ajusta-os (stretch)
 * TitlePane - idem com ligeiras diferen�as.
 * AnchororPane - permite fixar objetos em v�rios pontos.
 * 
 * Site : http://docs.oracle.com/javafx/2/layout/builtin_layouts.htm#CHDGHCDG
 * 
 * Nesta atividade vamos abordar o Layout BorderPane, que tem Regi�es
 * 		- Para a regi�o top, vamps simular  um menu de 3 botoes num Hbox layout
 * 		- Para a regi�o esquerda, vamos usar 2 bot�es num VBox layout
 * */

public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			// top layout
			HBox layoutTop = new HBox(10);			// Cria o layout HBox com 10px espacamento entre objetos
			layoutTop.setPadding(new Insets(15,12,15,12)); //Distancia deste layout ao root layout
			layoutTop.setStyle("-fx-background-color: #336699;");  //Defini��o da cor
					//--3 BOTOES-----
			
			Button btnTopMenu1 = new Button("File");
			Button btnTopMenu2 = new Button("Edit");
			Button btnTopMenu3 = new Button("View");
			
			layoutTop.getChildren().addAll(btnTopMenu1,btnTopMenu2,btnTopMenu3); //Adicionados ao HBox
			
			//leftLayout - Menu							//O mesmo para o VBox
			VBox layoutLeft = new VBox(10);
			layoutLeft.setPadding(new Insets(15,12,15,12));
			layoutLeft.setStyle("-fx-background-color: #336633;");
			
			Button btnLeftMenu1 = new Button("op��o1");
			Button btnLeftMenu2 = new Button("op��o2");
			Button btnLeftMenu3 = new Button("op��o3");
			layoutLeft.getChildren().addAll(btnLeftMenu1,btnLeftMenu2,btnLeftMenu3);
			
			
			//Adicionar os layouts anteriores, j� montados, �s regi�es do BorderPane
			
			BorderPane layoutRoot = new BorderPane();
			layoutRoot.setTop(layoutTop);
			layoutRoot.setLeft(layoutLeft);
			
			Scene scene = new Scene(layoutRoot,400,600);
		
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
